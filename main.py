from flask import Flask, render_template, request,redirect,Response
from werkzeug import secure_filename
import time,flask,json,base64,pdb,datetime
from pythonfiles.process import Identify
import pythonfiles.googlevision as vision
from pymongo import MongoClient

app = Flask(__name__,static_folder="images")

client = MongoClient('ds145019.mlab.com',45019)
db = client['alpr']
db.authenticate('gelato','gelato')

vehicle = db['vehicles']
userdata = db['userdata']
reports = db['reports']

def get_random():
	return str(int(time.time()))

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/sendpic', methods = ['POST'])
def sendpic():
	if request.method == 'POST':
		imgstring = request.values.get('file')
		imgstring = str.encode(imgstring)
		# print(imgstring)
		with open("images/imageToSave.jpg", "wb") as fh:
			fh.write(base64.decodestring(imgstring))
		identify = Identify()
		ans = identify.start_normal("images/imageToSave.jpg")
		return flask.jsonify(**ans)

@app.route('/plateonly', methods = ['POST'])
def plateonly():
	if request.method == 'POST':
		algo = int (request.values.get('algo'))
		
		imgstring = request.values.get('file')
		imgstring = str.encode(imgstring)
		
		platepath = "images/plate.jpg"
		
		with open(platepath, "wb") as fh:
			fh.write(base64.decodestring(imgstring))
		identify = Identify()
		ans = identify.plate_no_frm_plt(platepath,algo)
		return flask.jsonify(**ans)

@app.route('/plateinfo', methods = ['POST'])
def plateinfo():
	if request.method == 'POST':
		number = request.values.get('number')
		ans = vehicle.find_one({"number":number}, {'_id': False})
		if (ans):
			ans['success'] = True
		else:
			ans = {'success':False }	
		print (ans)
		return flask.jsonify(**ans)

@app.route('/login', methods = ['POST'])
def login():
	if request.method == 'POST':
		username = request.values.get('username')
		password = request.values.get('password')
		ans = userdata.find_one({"username":username,"password":password}, {'_id': False,'password':False})
		if (ans):
			ans['success'] = True
		else:
			ans = {'success':False }	
		return flask.jsonify(**ans)

@app.route('/reportpol', methods = ['POST'])
def reportpol():
	if request.method == 'POST':
		data = request.values.get('data')
		temp = json.loads(data)
		reporttext = temp['reporttext']
		fine = json.loads(temp['fine'])
		number = temp['number']
		uid = temp['id']
		timestamp = datetime.datetime.utcnow()+datetime.timedelta(minutes=330)
		timestamp = timestamp.strftime("%d-%m-%Y %H:%M:%S")
		for i in fine:
			i['timestamp'] = timestamp
		if (reporttext):
			reports.insert_one({"userid":uid,"text":reporttext,"number":number})
		print (fine)
		vehicle.update({'number' : number}, { '$push':{'challan': {'$each':fine}}})	
		ans = {}
		ans['success'] = True	
		return flask.jsonify(**ans)

@app.route('/reportguest', methods = ['POST'])
def reportguest():
	if request.method == 'POST':
		data = request.values.get('data')
		
		# ans = userdata.find_one({"username":username,"password":password}, {'_id': False,'password':False})
		ans = {}
		if (ans):
			ans['success'] = True
		else:
			ans = {'success':False }	
		return flask.jsonify(**ans)

if __name__ == '__main__':
	app.run(host= '0.0.0.0', port=5000, debug=True)