import numpy as np ,os,math,cv2
import matplotlib
matplotlib.use('Agg')
import numpy as np
import cv2
import os
from skimage.morphology import disk
from scipy.interpolate import spline
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import argparse

robertX = np.array((
	[1, 0],
	[0, -1]), dtype="uint8")

robertY = np.array((
	[0, 1],
	[-1, 0]), dtype="uint8")

sobelX = np.array((
	[-1, 0, 1],
	[-2, 0, 2],
	[-1, 0, 1]), dtype="uint8")

diamond = np.array((
	[0, 1, 0],
	[1, 0, 1],
	[0, 1, 0]), dtype="uint8")

def projection(img):
	a = np.zeros((img.shape[0],1))
	for i in range(img.shape[1]):
		a[i] = img[i].sum()/img.shape[0]
	return a
def resize(img,y):
	ratio = float(img.shape[1]) / float(img.shape[0])
	x = int(ratio * y)
	if (y>img.shape[1]):
		return img
	else:
		return cv2.resize(img, (x,y))

# def global_threshold(img):
# 	100 + 20U log[Is(x, y) + 1]

def median_80(img):
	x,y = int(img.shape[1]*0.2),3
	return cv2.medianBlur(img,5)

def convulution(img,kernel):
	return cv2.filter2D(img,-1,kernel)

def sobel_hor(img):
	return convulution(img,sobelX.transpose())

def sobel_ver(img):
	return convulution(img,sobelX)

def robert_hor(img):
	return convulution(img,robertX)

def robert_ver(img):
	return convulution(img,robertY)	

def canny(img):
	return cv2.Canny(img, 00,10)

def sobeltotal(img):
	a = sobelhorizontal(img)
	b = sobelvertical(img)
	c = (a*a) + (b*b)
	return np.sqrt(c)

def histogram_equalization(img):
	return cv2.equalizeHist(img)

def convert_to_grey(img):
	return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def otsu_binary(img):
	return cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)


def vertical_pro(img,output):
	canvas = plt.figure()
	rect = canvas.patch
	rect.set_facecolor('white')

	x = list(range(img.shape[0]))
	y = projection(img).tolist()
	x_sm = np.array(x)
	y_sm = np.array(y)

	x_smooth = np.linspace(x_sm.min(), x_sm.max(), 200)
	y_smooth = spline(x, y, x_smooth)

	# Define the matrix of 1x1 to place subplots
	# Placing the plot1 on 1x1 matrix, at pos 1
	sp1 = canvas.add_subplot(1,1,1, axisbg='w')
	#sp1.plot(x, y, 'red', linewidth=2)
	sp1.plot(y_smooth,x_smooth , 'red', linewidth=1)

	# Colorcode the tick tabs 
	sp1.tick_params(axis='x', colors='red')
	sp1.tick_params(axis='y', colors='red')

	# Colorcode the spine of the graph
	sp1.spines['bottom'].set_color('r')
	sp1.spines['top'].set_color('r')
	sp1.spines['left'].set_color('r')
	sp1.spines['right'].set_color('r')

	# Put the title and labels
	sp1.set_title('matplotlib example 1', color='red')
	sp1.set_xlabel('matplot x label', color='red')
	sp1.set_ylabel('matplot y label', color='red')

	# Show the plot/image
	plt.tight_layout()
	plt.grid(alpha=0.8)
	plt.savefig(output)

def predict_char(img,model):

	size = 28
	dic = {0:'0'}
	for i in range(10):
		dic[i]=str(i)
	for i in range(26):
		dic[i+10] = chr(65+i)
	tex = []
	new = cv2.resize(img, (size, size))
	new = 255-new
	tex.append(new)
	X_test=np.asarray(tex,dtype=np.uint8)
	X_test = X_test.reshape(X_test.shape[0], 1, size, size).astype('float32')
	X_test = X_test / 255
	ans = np.argmax(model.predict(X_test[0:1]))
	return (dic[ans])
def sort_contours(cnts, method="left-to-right"):
	# initialize the reverse flag and sort index
	reverse = False
	i = 0
 
	# handle if we need to sort in reverse
	if method == "right-to-left" or method == "bottom-to-top":
		reverse = True
 
	# handle if we are sorting against the y-coordinate rather than
	# the x-coordinate of the bounding box
	if method == "top-to-bottom" or method == "bottom-to-top":
		i = 1
 
	# construct the list of bounding boxes and sort them from top to
	# bottom
	boundingBoxes = [cv2.boundingRect(c) for c in cnts]
	(cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
		key=lambda b:b[1][i], reverse=reverse))
 
	# return the list of sorted contours and bounding boxes
	return (cnts, boundingBoxes)

