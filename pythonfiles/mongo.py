from pymongo import MongoClient
client = MongoClient('ds145019.mlab.com',45019)
db = client['alpr']
db.authenticate('gelato','gelato')
collection = db['vehicles']
collection.drop()
vehicle = {
	"number" : "PB11BH2332",
	"owner"	 : [ { "name":"Anubhav","start":2017,"end":2019,"current":False},{ "name":"Ankit","start":2020,"end":-1,"current":True} ],
	"stolen" : {"state":True,"fir":"Lost at telephone booth"},
	"challan"  : [ {
            "timestamp" : "123456781",
            "purpose" : "Overspeeding",
            "fine" : 100,
            "paid" : True
        }, 
        {
            "timestamp" : "123456781",
            "purpose" : "Red Light",
            "fine" : 100,
            "paid" : False
        }, 
        {
            "timestamp" : "123456781",
            "purpose" : "Grey",
            "fine" : 100,
            "paid" : False
        } ],
	"model"  : "Honda-City VXI",
	"registration" : "AP3124414123",
	"color"  : "blue"
}

collection.insert_one(vehicle)

collection = db['userdata']
collection.drop()
userdata = {
	"id" : "12412125",
	"username"	 : "admin",
	"password" : "password"
}

collection.insert_one(userdata)
