import re,numpy as np,os,cv2,csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K
from keras.datasets import mnist
K.set_image_dim_ordering('th')

size = 28
par = os.path.abspath((os.path.join(os.getcwd(),'fonts')))
print (par)
lix = []
liy = []
count = 0
for i in os.listdir(par):
	if '.DS_Store' in i:
		continue
	if count == 36:
		break	
	fold = os.path.join(par,i)	
	for j in os.listdir(fold):
		path = os.path.join(fold,j)
		img = cv2.imread(path)
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		new = cv2.resize(img, (size, size))
		lix.append(255-new)
		liy.append(count)
		fin = new.tolist()

	count += 1
	
X_train=np.asarray(lix,dtype=np.uint8)
y_train=np.asarray(liy,dtype=np.uint8)	
(xt, yt), (X_test, y_test) = mnist.load_data()
seed = 7
np.random.seed(seed)

num_pixels = X_train.shape[1] * X_train.shape[2]
# reshape to be [samples][pixels][width][height]

########
X_train = X_train.reshape(X_train.shape[0], 1, size, size).astype('float32')
# X_train = X_train.reshape(X_train.shape[0], num_pixels).astype('float32')
########

# normalize inputs from 0-255 to 0-1
X_train = X_train / 255
# one hot encode outputs
y_train = np_utils.to_categorical(y_train)
num_classes = y_train.shape[1]
# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(num_pixels, input_dim=num_pixels, init='normal', activation='relu'))
	model.add(Dense(num_classes, init='normal', activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

def larger_model():
	# create model
	model = Sequential()
	model.add(Convolution2D(30, 5, 5, border_mode='valid', input_shape=(1, size, size), activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Convolution2D(15, 3, 3, activation='relu'))
	model.add(MaxPooling2D(pool_size=(2, 2)))
	model.add(Dropout(0.2))
	model.add(Flatten())
	model.add(Dense(128, activation='relu'))
	model.add(Dense(50, activation='relu'))
	model.add(Dense(num_classes, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model

model = larger_model()
model.fit(X_train, y_train, nb_epoch=30, batch_size=200, verbose=2)
