import numpy as np
import cv2
import os
from pythonfiles.Preprocess import preprocess
import pythonfiles.resources as resources
from keras.models import load_model
import copy,pdb
import pythonfiles.googlevision as vision

showImages = True
thickness = 8
extra = 0.01
algo = 1

class Identify:
	
	def __init__(self):
		self.model = load_model('models/30epoch')
		self.dic = {0:'0'}
		for i in range(10):
			self.dic[i]=str(i)
		for i in range(26):
			self.dic[i+10] = chr(65+i)

	def start_normal(self, filepath):
		ans = {'number':'','status':0,'msg':''}
		ori_image = cv2.imread(filepath)
		ori_imgray,ori_binarized = self.pre_process(ori_image)
		actual_plate,localized_plate_img = self.extract_num_plate(ori_imgray,ori_binarized)
		x,y,w,h = cv2.boundingRect(actual_plate)
		ans['x1'] = x
		ans['x2'] = x + w
		ans['y1'] = y
		ans['y2'] = y + h
		if localized_plate_img is None:
			print ('License Plate not extracted')
			ans['msg'] = 'License Plate not extracted'
			ans['status'] = 1
			return ans
		cv2.imwrite('images/implate.jpg',localized_plate_img,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
		extra = self.plate_no_frm_plt("images/implate.jpg",algo)
		ans.update(extra)
		return (ans)

	def pre_process(self,img):
		#Returns gray and binarized img
		return preprocess(img)

	def extract_num_plate(self,gray,binarized):
		
		possible_char = []
		possible_plate = []
		actual_plate = None

		m,n = binarized.shape
		total_pix = m*n
		im2, contours, hierarchy = cv2.findContours(binarized,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
		if (showImages):
			# cv2.imwrite('imoriginal.jpg',self.ori_image,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
			cv2.imwrite('images/imgray.jpg',gray,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
			cv2.imwrite('images/imbinarized.jpg',binarized,[int(cv2.IMWRITE_JPEG_QUALITY), 10])

		for cnt in contours:
			x,y,w,h = cv2.boundingRect(cnt)
			area = w*h
			if area<total_pix/10000:
				continue
			ap = w/h
			if ap<0.8 and ap > 0.4:
				possible_char.append(cnt)
				cv2.rectangle(binarized,(x,y),(x+w,y+h),(255,255,255),3)
		for cnt in contours:
			x,y,w,h = cv2.boundingRect(cnt)
			area = w*h
			if area<total_pix/1000:
				continue
			# if area>500:
			# 	continue	
			ap = w/h
			if ap>1.5 and ap<5:
				possible_plate.append(cnt)
				cv2.rectangle(binarized,(x,y),(x+w,y+h),(255,255,255),thickness)
		if (showImages):
			cv2.imwrite('images/imbinarized2.jpg',binarized,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
		maxstr = 0
		for cnt in possible_plate:

			x,y,w,h = cv2.boundingRect(cnt)
			count = 0
			for cntch in possible_char:
				x1,y1,w1,h1 = cv2.boundingRect(cntch)
				if ((x1+x1+w1)/2) > x and ((x1+x1+w1)/2) < x+w and ((y1+y1+h1)/2) > y and ((y1+y1+h1)/2) < y+h:
					if (w*h)/(w1*h1)<50:
						count += 1
			if count>=4 and count<25:
				if count>maxstr:
					actual_plate = cnt
					maxstr = count
		print(type(actual_plate))
		if actual_plate == None:
			return (None,None)			
		crop_img = []
		x,y,w,h = cv2.boundingRect(actual_plate)
		crop_img = gray[int((1-extra)*y):int((1+extra)*(y+h)),int((1-extra)*x):int((1+extra)*(x+w))]					
		if (showImages):
			cv2.imwrite('images/implate.jpg',crop_img,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
		return (actual_plate,crop_img)
	def process_plate(self,img):

		ret2,binarized = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
		im2, contours, hierarchy = cv2.findContours(binarized,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
		charcnt = []
		# print (len(contours))
		m,n = binarized.shape
		total_pix = m*n
		for cnt in contours:
			x,y,w,h = cv2.boundingRect(cnt)
			area = w*h
			# print (area)
			if area<total_pix/100 or area>total_pix/10 or w>h:
				continue
			else:
				charcnt.append(cnt)
		chars = []
		for l, cnt in enumerate(charcnt):
			x,y,w,h = cv2.boundingRect(cnt)
			for j, cntch in enumerate(charcnt):
				x1,y1,w1,h1 = cv2.boundingRect(cntch)
				if  x1 > x and x1+w1 < x+w and y1 > y and y1+h1 < y+h:
					chars.append(j)
		chars = list(set(chars))
		chars = sorted(chars, reverse=True)
		for t in chars:
			charcnt.pop(t)
		text = []	
		(charcnt, boundingBoxes) = resources.sort_contours(charcnt)
		return charcnt
	def predict_char(self,img,model):

		size = 28
		temp = []
		resized = cv2.resize(img, (size, size))
		inverse = 255-resized
		temp.append(inverse)
		X_test=np.asarray(temp,dtype=np.uint8)
		X_test = X_test.reshape(X_test.shape[0], 1, size, size).astype('float32')
		X_test = X_test / 255
		# pdb.set_trace()

		ans = np.argmax(model.predict(X_test[0:1]))
		return (self.dic[ans])
	def plate_no_frm_plt(self,platepath,algo):
		ans = {}
		#Google
		if (algo == 1):
			text = vision.gettext(platepath)
			
		#My own
		else:
			plate_image = cv2.imread(platepath,0)
			charcnt = self.process_plate(plate_image)
			print (len(charcnt))
			text = []
			for cnt in charcnt:
			
				x,y,w,h = cv2.boundingRect(cnt)
				# print (x,y,w,h)
				# cv2.rectangle(crop_img,(x,y),(x+w,y+h),(255,0,0),2)
				t_img = plate_image[y:y+h,x:x+w]
				t_img = resources.otsu_binary(t_img)[1]
				t_img = cv2.resize(t_img, (28, 28))
				cv2.imwrite('images/imtimg.jpg',t_img,[int(cv2.IMWRITE_JPEG_QUALITY), 10])
				text.append(self.predict_char(t_img,self.model))
		text = ''.join(text)
		ans['msg'] = 'Successfully done'
		ans['status'] = 2
		ans['number'] = text
		print (ans)
		return ans
			

# a = Identify('/Users/Viper/Workbench/btp/images/images-collection/images-12mp/IMG_20160930_152416.jpg')
# print (a)